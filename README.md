# Foodyfi

## Introduction

According to the recent survey, in the starting month of February, 46% of respondents reported their store was lower compared to the same period in 2019. However, there were only a few confirmed COVID-19 cases at that time in Canada. 24% of pf respondents said that their business was negatively impacted by COVID-19. While in the last two weeks of March, 96% of respondents reported same-store sales compared to the same period in 2019. At the end of March, social distancing measures are imposed by the government. As a result, 53% of respondents closed their entire operation temporarily. While other restaurants remain open for delivery and takeout, and many have laid-off workers or cut back staff to zero.
Restaurants should begin making plans for alternative ways to serve food. Some of the plans include delivery service, mobile food trucks that deliver prepackaged meals, and takeaways of regular menu items. This makes sense and restaurateurs should begin the process now to survive the current crisis and whatever follows. By taking advantage of any tax incentives or any government-aid programs and special benefits available restaurant owners and their employees to help them.
Without going into the restaurant, people can order food through online food service applications like door dash, uber eats. And, by hiring more online delivery guys can help in delivering food on time.


---

## Technology Listing:

* Node.js
* MongoDB
* HTML
* CSS
* JQuery




---

## Prerequisites

* Visual Studio Code - https://code.visualstudio.com/download
* Node.js - https://nodejs.org/en/download/
* MongoDB - https://www.mongodb.com/try/download/community

---

## Setup

### Install Dependency 

```
npm install
```

### Start Server

```
npm server
```

### Create a package.json file

```
npm init
```

### Run Application

```
node home.ejs
```

### Access Web Browser

```
http://localhost:3000/
```
---
# License
Copyright (c) 2020 @Bhaskar Reddy Kandhadi. 
Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.